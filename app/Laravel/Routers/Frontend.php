<?php


/**
 *
 * ------------------------------------
 * Frontend Routes
 * ------------------------------------
 *
 */

$this->group(

	array(
		'as' => "frontend.",
		'namespace' => "Frontend",
	),

	function() {

		$this->get('/',['as' => "index",'uses' => "PageController@index"]);
		$this->get('aba',['as' => "aba",'uses' => "PageController@aba"]);
		$this->get('abis',['as' => "abis",'uses' => "PageController@abis"]);
		$this->get('asean-bac',['as' => "asean_bac",'uses' => "PageController@asean_bac"]);
		$this->get('members',['as' => "members",'uses' => "PageController@members"]);
		$this->get('activities',['as' => "activities",'uses' => "PageController@activities"]);
		$this->get('publication',['as' => "publication",'uses' => "PageController@publication"]);
		$this->get('contact',['as' => "contact",'uses' => "PageController@contact"]);
		$this->post('contact',['uses' => "PageController@sent"]);
		$this->post('subscription',['as' => "subscription",'uses' => "PageController@subscribe"]);
		$this->any('unsubscribe/{email}',['as' => "unsubscribe",'uses' => "PageController@unsubscribe"]);

		$this->group(['prefix' => "partners", 'as' => "partners."], function () {
			$this->get('/{type}',['as' => "index", 'uses' => "PartnerController@index"]);
		});

		$this->group(['prefix' => "events", 'as' => "events."], function () {
			$this->get('/',['as' => "index", 'uses' => "EventsController@index"]);
			$this->get('show/{id}',['as' => "show", 'uses' => "EventsController@show"]);
		});

		$this->group(['prefix' => "news", 'as' => "news."], function () {
			$this->get('/',['as' => "index", 'uses' => "NewsController@index"]);
			$this->get('show/{id}',['as' => "show", 'uses' => "NewsController@show"]);
		});

		$this->group(['prefix' => "gallery", 'as' => "gallery."], function () {
			$this->get('/',['as' => "index", 'uses' => "GalleryController@index"]);
			$this->get('show/{id}',['as' => "show", 'uses' => "GalleryController@show"]);
		});
	}
);