<?php


/**
 *
 * ------------------------------------
 * Backoffice Routes
 * ------------------------------------
 *
 */

Route::group(

	array(
		'as' => "backoffice.",
		'prefix' => "admin",
		'namespace' => "Backoffice",
	),

	function() {

		$this->group(['as'=>"auth.", 'middleware' => ["web","backoffice.guest"]], function(){
			$this->get('login',['as' => "login",'uses' => "AuthController@login"]);
			$this->post('login',['uses' => "AuthController@authenticate"]);
		});

		$this->group(['middleware' => "backoffice.auth"], function(){

			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);
			$this->get('voters',['as' => "voters",'uses' => "VotersController@index"]);
			$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);


			$this->group(['prefix' => "profile", 'as' => "profile."], function () {
				$this->get('/',['as' => "settings", 'uses' => "ProfileController@setting"]);
				$this->post('/',['uses' => "ProfileController@update_setting"]);
				$this->post('update-password',['as' => "update_password",'uses' => "ProfileController@update_password"]);
			});
 

			$this->group(['prefix' => "timein", 'as' => "timein."], function () {
				$this->get('/',['as' => "index", 'uses' => "AttendanceController@index"]);
				$this->get('create',['as' => "create", 'uses' => "AttendanceController@create"]);
				$this->post('create',['uses' => "AttendanceController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AttendanceController@edit"]);
				$this->post('edit/{id?}',['uses' => "AttendanceController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EmployeeController@destroy"]);
			});


			$this->group(['prefix' => "employees", 'as' => "employee."], function () {
				$this->get('/',['as' => "index", 'uses' => "EmployeeController@index"]);
				$this->get('create',['as' => "create", 'uses' => "EmployeeController@create"]);
				$this->post('create',['uses' => "EmployeeController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "EmployeeController@edit"]);
				$this->post('edit/{id?}',['uses' => "EmployeeController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EmployeeController@destroy"]);
			});

			$this->group(['prefix' => "leave", 'as' => "leave."], function () {
				$this->get('/',['as' => "index", 'uses' => "LeaveController@index"]);
				$this->get('create',['as' => "create", 'uses' => "LeaveController@create"]);
				$this->post('create',['uses' => "LeaveController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "LeaveController@edit"]);
				$this->post('edit/{id?}',['uses' => "LeaveController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "LeaveController@destroy"]);
			});

			$this->group(['prefix' => "news", 'as' => "news."], function () {
				$this->get('/',['as' => "index", 'uses' => "NewsController@index"]);
				$this->get('create',['as' => "create", 'uses' => "NewsController@create"]);
				$this->post('create',['uses' => "NewsController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "NewsController@edit"]);
				$this->post('edit/{id?}',['uses' => "NewsController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "NewsController@destroy"]);
			});

			$this->group(['prefix' => "partners", 'as' => "partners."], function () {
				$this->get('/',['as' => "index", 'uses' => "PartnerController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PartnerController@create"]);
				$this->post('create',['uses' => "PartnerController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PartnerController@edit"]);
				$this->post('edit/{id?}',['uses' => "PartnerController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PartnerController@destroy"]);
			});

			$this->group(['prefix' => "flags", 'as' => "flags."], function () {
				$this->get('/',['as' => "index", 'uses' => "FlagController@index"]);
				$this->get('create',['as' => "create", 'uses' => "FlagController@create"]);
				$this->post('create',['uses' => "FlagController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "FlagController@edit"]);
				$this->post('edit/{id?}',['uses' => "FlagController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FlagController@destroy"]);
			});

			$this->group(['prefix' => "albums", 'as' => "albums."], function () {
				$this->get('/',['as' => "index", 'uses' => "AlbumController@index"]);
				$this->get('create',['as' => "create", 'uses' => "AlbumController@create"]);
				$this->post('create',['uses' => "AlbumController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AlbumController@edit"]);
				$this->post('edit/{id?}',['uses' => "AlbumController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AlbumController@destroy"]);
			});

			$this->group(['prefix' => "gallery", 'as' => "gallery."], function () {
				$this->get('/',['as' => "index", 'uses' => "GalleryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "GalleryController@create"]);
				$this->post('create',['uses' => "GalleryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "GalleryController@edit"]);
				$this->post('edit/{id?}',['uses' => "GalleryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "GalleryController@destroy"]);
			});

			$this->group(['prefix' => "video-gallery", 'as' => "video_gallery."], function () {
				$this->get('/',['as' => "index", 'uses' => "VideoGalleryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "VideoGalleryController@create"]);
				$this->post('create',['uses' => "VideoGalleryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "VideoGalleryController@edit"]);
				$this->post('edit/{id?}',['uses' => "VideoGalleryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "VideoGalleryController@destroy"]);
			});

			$this->group(['prefix' => "featured-videos", 'as' => "featured_video."], function () {
				$this->get('/',['as' => "index", 'uses' => "FeaturedVideoController@index"]);
				$this->get('create',['as' => "create", 'uses' => "FeaturedVideoController@create"]);
				$this->post('create',['uses' => "FeaturedVideoController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "FeaturedVideoController@edit"]);
				$this->post('edit/{id?}',['uses' => "FeaturedVideoController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FeaturedVideoController@destroy"]);

				$this->get('all-featured-videos',['as' => "all", 'uses' => "FeaturedVideoController@all"]);
			});
			
			$this->group(['prefix' => "events", 'as' => "events."], function () {
				$this->get('/',['as' => "index", 'uses' => "EventController@index"]);
				$this->get('create',['as' => "create", 'uses' => "EventController@create"]);
				$this->post('create',['uses' => "EventController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "EventController@edit"]);
				$this->post('edit/{id?}',['uses' => "EventController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "EventController@destroy"]);
			});
			
			$this->group(['prefix' => "supporting-organization", 'as' => "supporting_organization."], function () {
				$this->get('/',['as' => "index", 'uses' => "SupportingOrganizationController@index"]);
				$this->get('create',['as' => "create", 'uses' => "SupportingOrganizationController@create"]);
				$this->post('create',['uses' => "SupportingOrganizationController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "SupportingOrganizationController@edit"]);
				$this->post('edit/{id?}',['uses' => "SupportingOrganizationController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SupportingOrganizationController@destroy"]);
			});
			
			$this->group(['prefix' => "press-releases", 'as' => "press_releases."], function () {
				$this->get('/',['as' => "index", 'uses' => "PressReleasesController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PressReleasesController@create"]);
				$this->post('create',['uses' => "PressReleasesController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PressReleasesController@edit"]);
				$this->post('edit/{id?}',['uses' => "PressReleasesController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PressReleasesController@destroy"]);
			});
			
			$this->group(['prefix' => "publications", 'as' => "publications."], function () {
				$this->get('/',['as' => "index", 'uses' => "PublicationController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PublicationController@create"]);
				$this->post('create',['uses' => "PublicationController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PublicationController@edit"]);
				$this->post('edit/{id?}',['uses' => "PublicationController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PublicationController@destroy"]);
			});
			
			$this->group(['prefix' => "page-content", 'as' => "page_content."], function () {
				$this->get('/',['as' => "index", 'uses' => "PageContentController@index"]);
				$this->get('create',['as' => "create", 'uses' => "PageContentController@create"]);
				$this->post('create',['uses' => "PageContentController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "PageContentController@edit"]);
				$this->post('edit/{id?}',['uses' => "PageContentController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "PageContentController@destroy"]);
			});
			
			$this->group(['prefix' => "members", 'as' => "members."], function () {
				$this->get('/',['as' => "index", 'uses' => "MemberController@index"]);
				$this->get('create',['as' => "create", 'uses' => "MemberController@create"]);
				$this->post('create',['uses' => "MemberController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "MemberController@edit"]);
				$this->post('edit/{id?}',['uses' => "MemberController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "MemberController@destroy"]);
			});
			
			$this->group(['prefix' => "header-images", 'as' => "header_images."], function () {
				$this->get('/',['as' => "index", 'uses' => "HeaderImageController@index"]);
				$this->get('create',['as' => "create", 'uses' => "HeaderImageController@create"]);
				$this->post('create',['uses' => "HeaderImageController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "HeaderImageController@edit"]);
				$this->post('edit/{id?}',['uses' => "HeaderImageController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "HeaderImageController@destroy"]);
			});
			
			$this->group(['prefix' => "social-links", 'as' => "social_links."], function () {
				$this->get('/',['as' => "index", 'uses' => "SocialLinkController@index"]);
				$this->get('create',['as' => "create", 'uses' => "SocialLinkController@create"]);
				$this->post('create',['uses' => "SocialLinkController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "SocialLinkController@edit"]);
				$this->post('edit/{id?}',['uses' => "SocialLinkController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "SocialLinkController@destroy"]);
			});
			
			$this->group(['prefix' => "advertisements", 'as' => "advertisements."], function () {
				$this->get('/',['as' => "index", 'uses' => "AdvertisementController@index"]);
				$this->get('create',['as' => "create", 'uses' => "AdvertisementController@create"]);
				$this->post('create',['uses' => "AdvertisementController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "AdvertisementController@edit"]);
				$this->post('edit/{id?}',['uses' => "AdvertisementController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AdvertisementController@destroy"]);
			});
			
			$this->group(['prefix' => "contact-inquiries", 'as' => "contact_inquiry."], function () {
				$this->get('/',['as' => "index", 'uses' => "ContactInquiryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "ContactInquiryController@create"]);
				$this->post('create',['uses' => "ContactInquiryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ContactInquiryController@edit"]);
				$this->post('edit/{id?}',['uses' => "ContactInquiryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ContactInquiryController@destroy"]);
			});
			
			$this->group(['prefix' => "logo", 'as' => "logo."], function () {
				$this->get('/',['as' => "index", 'uses' => "LogoController@edit"]);
				$this->post('/',['uses' => "LogoController@update"]);
			});
			
		});
	}
);