<?php 

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\User;
use App\Laravel\Models\FeaturedVideo;
use App\Laravel\Models\News;
use App\Laravel\Models\Album;
use App\Laravel\Models\Event;
use App\Laravel\Models\Flag;
use App\Laravel\Models\SocialLink;
use App\Laravel\Models\HeaderImage;
use App\Laravel\Models\PageContent;
use App\Laravel\Models\SupportingOrganization;
use App\Laravel\Models\PressReleases;
use App\Laravel\Models\Publication;
use App\Laravel\Models\Advertisement;
use App\Laravel\Models\Logo;

use Illuminate\Support\Collection;
use App\Http\Controllers\Controller as MainController;
use Auth, Session,Carbon, Helper,Route,DNS2D;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_featured_video();
		self::set_news();
		self::set_albums();
		self::set_events();
		self::set_flags();
		self::set_social_links();
		self::set_header_images();
		self::set_page_contents();
		self::set_supporting_organization();
		self::set_press_releases();
		self::set_publications();
		self::set_advertisements();
		self::set_similars();
		self::set_logo();
	}

	public function get_data(){
		return $this->data;
	}

	public function set_featured_video(){
		$this->data['featured_videos'] = FeaturedVideo::orderBy('created_at','DESC')->paginate(2);
	}

	public function set_news(){
		$this->data['news'] = News::orderBy('posted_at','DESC')->where('is_featured','no')->paginate(4);
		$this->data['featured_news'] = News::orderBy('posted_at','DESC')->where('is_featured','yes')->paginate(4);
		$this->data['side_bar_news'] = News::orderBy('posted_at','DESC')->paginate(6);
	}

	public function set_albums(){
		$this->data['albums'] = Album::orderBy('created_at','DESC')->paginate(4);
	}

	public function set_events(){
		$this->data['events'] = Event::orderBy('created_at','DESC')->paginate(4);
	}

	public function set_flags(){
		$this->data['flags'] = Flag::all();
	}

	public function set_social_links(){
		$this->data['social_links'] = SocialLink::all();
	}

	public function set_header_images(){
		$this->data['header_images'] = HeaderImage::all();
	}

	public function set_page_contents(){
		$this->data['page_content'] = PageContent::all();
	}

	public function set_supporting_organization(){
		$this->data['organizations'] = SupportingOrganization::all();
	}

	public function set_press_releases(){
		$this->data['press_releases'] = PressReleases::all();
		$this->data['footer_press_releases'] = PressReleases::orderBy('created_at','DESC')->paginate(6);
	}

	public function set_publications(){
		$this->data['publications'] = Publication::all();
	}

	public function set_advertisements(){
		$this->data['advertisements'] = Advertisement::all();
	}

	public function set_similars(){
		$this->data['similar_posts'] = News::orderBy('created_at','DESC')->paginate(3);
		$this->data['similar_albums'] = Album::orderBy('created_at','DESC')->paginate(3);
	}

	public function set_logo(){
		$this->data['logo'] = Logo::orderBy('created_at','DESC')->first()? : new Logo;
	}
}