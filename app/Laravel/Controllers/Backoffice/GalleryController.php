<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Gallery;
use App\Laravel\Models\Album;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\GalleryRequest;
use App\Laravel\Requests\Backoffice\EditGalleryRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class GalleryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Gallery";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "gallery";

		$this->data['albums'] = ['' => "Choose an album for this image"] + Album::all()->pluck('title','id')->toArray();

	}

	public function index () {
		$this->data['images'] = Gallery::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (GalleryRequest $request) {
		try {
			$new_gallery = new Gallery;
			$new_gallery->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/Gallery');
				$new_gallery->path = $upload["path"];
				$new_gallery->directory = $upload["directory"];
				$new_gallery->filename = $upload["filename"];
			}

			if($new_gallery->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New image on the gallery has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$gallery = Gallery::find($id);

		if (!$gallery) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['gallery'] = $gallery;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditGalleryRequest $request, $id = NULL) {
		try {
			$gallery = Gallery::find($id);

			if (!$gallery) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$gallery->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/Gallery');
				if($upload){	
					if (File::exists("{$gallery->directory}/{$gallery->filename}")){
						File::delete("{$gallery->directory}/{$gallery->filename}");
					}
					if (File::exists("{$gallery->directory}/resized/{$gallery->filename}")){
						File::delete("{$gallery->directory}/resized/{$gallery->filename}");
					}
					if (File::exists("{$gallery->directory}/thumbnails/{$gallery->filename}")){
						File::delete("{$gallery->directory}/thumbnails/{$gallery->filename}");
					}
				}
				
				$gallery->path = $upload["path"];
				$gallery->directory = $upload["directory"];
				$gallery->filename = $upload["filename"];
			}

			if($gallery->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A image on the gallery has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$gallery = Gallery::find($id);

			if (!$gallery) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$gallery->directory}/{$gallery->filename}")){
				File::delete("{$gallery->directory}/{$gallery->filename}");
			}
			if (File::exists("{$gallery->directory}/resized/{$gallery->filename}")){
				File::delete("{$gallery->directory}/resized/{$gallery->filename}");
			}
			if (File::exists("{$gallery->directory}/thumbnails/{$gallery->filename}")){
				File::delete("{$gallery->directory}/thumbnails/{$gallery->filename}");
			}

			if($gallery->save() AND $gallery->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A image on the gallery has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}