<?php 

namespace App\Laravel\Controllers\Backoffice;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\Attendance;
use App\User;

/*
*
* Requests used for validating inputs
*/

use App\Laravel\Requests\Backoffice\AttendanceRequest;


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB;

class DashboardController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		//$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Attendance";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "dashboard";
		$this->data['date'] = "dashboard";
		$this->data['time'] = "dashboard";
		$this->data['remarks'] = "dashboard";
	}
	public function store (AttendanceRequest $request) {
		try {
			$new_attendance = new Attendance;
			$new_attendance->fill($request->all());


			 
			if($new_attendance->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New employee has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}
				
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function index () {
		$this->data['attendance'] = Attendance::orderBy('created_at',"DESC")->get();
		return view('backoffice.dashboard',$this->data);
	}

	public function icons () {
		return view('backoffice.icons',$this->data);
	}
}