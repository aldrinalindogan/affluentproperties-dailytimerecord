<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Attendance;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AttendanceRequest; 
/**
*
* Classes used for this controller
*/
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image;

class AttendanceController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Attendance Record";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "timein";
		$this->data['date'] = date('j-F-Y');
		$this->data['time'] = date('H:i:s');
		$this->data['remarks'] = "On-Going"; 		
		

		 
	}

	public function index () {
		$this->data['attendance'] = Attendance::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (AttendanceRequest $request) {
		try {
			$new_employee = new Attendance;
			$new_employee->fill($request->all());


			 
			if($new_employee->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New employee has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}
				
			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$attendance = Attendance::find($id);

		if (!$attendance) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['attendance'] = $attendance;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditEmployeeRequest $request, $id = NULL) {
		try {
			$attendance = Attendance::find($id);

			if (!$attendance) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$attendance->fill($request->all());

			if($request->has('password')){
				$attendance->password =  bcrypt($request->get('password'));
			}

			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				if($upload){	
					if (File::exists("{$employee->directory}/{$employee->filename}")){
						File::delete("{$employee->directory}/{$employee->filename}");
					}
					if (File::exists("{$employee->directory}/resized/{$employee->filename}")){
						File::delete("{$employee->directory}/resized/{$employee->filename}");
					}
					if (File::exists("{$employee->directory}/thumbnails/{$employee->filename}")){
						File::delete("{$employee->directory}/thumbnails/{$employee->filename}");
					}
				}
				
				$attendance->directory = $upload["directory"];
				$attendance->filename = $upload["filename"];
			}

			if($attendance->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An employee has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
  
}