<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\PressReleases;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\PressReleasesRequest;
use App\Laravel\Requests\Backoffice\EditPressReleasesRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class PressReleasesController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Press Releases";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "press_releases";
	}

	public function index () {
		$this->data['press_releases'] = PressReleases::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (PressReleasesRequest $request) {
		try {
			$new_release = new PressReleases;
			$new_release->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/press_releases');
				$new_release->path = $upload["path"];
				$new_release->directory = $upload["directory"];
				$new_release->filename = $upload["filename"];
			}

			if($new_release->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New press release has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$press_releases = PressReleases::find($id);

		if (!$press_releases) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['press_releases'] = $press_releases;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditPressReleasesRequest $request, $id = NULL) {
		try {
			$press_releases = PressReleases::find($id);

			if (!$press_releases) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$press_releases->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/press_releases');
				if($upload){	
					if (File::exists("{$press_releases->directory}/{$press_releases->filename}")){
						File::delete("{$press_releases->directory}/{$press_releases->filename}");
					}
					if (File::exists("{$press_releases->directory}/resized/{$press_releases->filename}")){
						File::delete("{$press_releases->directory}/resized/{$press_releases->filename}");
					}
					if (File::exists("{$press_releases->directory}/thumbnails/{$press_releases->filename}")){
						File::delete("{$press_releases->directory}/thumbnails/{$press_releases->filename}");
					}
				}
				
				$press_releases->path = $upload["path"];
				$press_releases->directory = $upload["directory"];
				$press_releases->filename = $upload["filename"];
			}

			if($press_releases->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A press release has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$press_releases = PressReleases::find($id);

			if (!$press_releases) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$press_releases->directory}/{$press_releases->filename}")){
				File::delete("{$press_releases->directory}/{$press_releases->filename}");
			}
			if (File::exists("{$press_releases->directory}/resized/{$press_releases->filename}")){
				File::delete("{$press_releases->directory}/resized/{$press_releases->filename}");
			}
			if (File::exists("{$press_releases->directory}/thumbnails/{$press_releases->filename}")){
				File::delete("{$press_releases->directory}/thumbnails/{$press_releases->filename}");
			}

			if($press_releases->save() AND $press_releases->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A press release has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}