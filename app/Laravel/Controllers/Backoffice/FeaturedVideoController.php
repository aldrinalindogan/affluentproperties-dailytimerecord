<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\FeaturedVideo;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\FeaturedVideoRequest;
use App\Laravel\Requests\Backoffice\EditFeaturedVideoRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class FeaturedVideoController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Featured Videos";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "featured_video";
	}

	public function index () {
		$this->data['videos'] = FeaturedVideo::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (FeaturedVideoRequest $request) {
		try {
			$new_video = new FeaturedVideo;
			$new_video->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/video');
				$new_video->path = $upload["path"];
				$new_video->directory = $upload["directory"];
				$new_video->filename = $upload["filename"];
			}

			if($new_video->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New video has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$video = FeaturedVideo::find($id);

		if (!$video) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['video'] = $video;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditFeaturedVideoRequest $request, $id = NULL) {
		try {
			$video = FeaturedVideo::find($id);

			if (!$video) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$video->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/video');
				if($upload){	
					if (File::exists("{$video->directory}/{$video->filename}")){
						File::delete("{$video->directory}/{$video->filename}");
					}
					if (File::exists("{$video->directory}/resized/{$video->filename}")){
						File::delete("{$video->directory}/resized/{$video->filename}");
					}
					if (File::exists("{$video->directory}/thumbnails/{$video->filename}")){
						File::delete("{$video->directory}/thumbnails/{$video->filename}");
					}
				}
				
				$video->path = $upload["path"];
				$video->directory = $upload["directory"];
				$video->filename = $upload["filename"];
			}

			if($video->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A video has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$video = FeaturedVideo::find($id);

			if (!$video) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$video->directory}/{$video->filename}")){
				File::delete("{$video->directory}/{$video->filename}");
			}
			if (File::exists("{$video->directory}/resized/{$video->filename}")){
				File::delete("{$video->directory}/resized/{$video->filename}");
			}
			if (File::exists("{$video->directory}/thumbnails/{$video->filename}")){
				File::delete("{$video->directory}/thumbnails/{$video->filename}");
			}

			if($video->save() AND $video->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A video has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function all () {
		$this->data['videos'] = FeaturedVideo::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.all',$this->data);
	}

}