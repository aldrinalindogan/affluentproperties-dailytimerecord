<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\HeaderImage;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\HeaderImageRequest;
use App\Laravel\Requests\Backoffice\EditHeaderImageRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class HeaderImageController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Header Image";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "header_images";
	}

	public function index () {
		$this->data['header_images'] = HeaderImage::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (HeaderImageRequest $request) {
		try {
			$new_header_image = new HeaderImage;
			$new_header_image->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/header_image');
				$new_header_image->path = $upload["path"];
				$new_header_image->directory = $upload["directory"];
				$new_header_image->filename = $upload["filename"];
			}

			if($new_header_image->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New header image has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$header_images = HeaderImage::find($id);

		if (!$header_images) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['header_image'] = $header_images;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditHeaderImageRequest $request, $id = NULL) {
		try {
			$header_images = HeaderImage::find($id);

			if (!$header_images) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$header_images->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/header_image');
				if($upload){	
					if (File::exists("{$header_images->directory}/{$header_images->filename}")){
						File::delete("{$header_images->directory}/{$header_images->filename}");
					}
					if (File::exists("{$header_images->directory}/resized/{$header_images->filename}")){
						File::delete("{$header_images->directory}/resized/{$header_images->filename}");
					}
					if (File::exists("{$header_images->directory}/thumbnails/{$header_images->filename}")){
						File::delete("{$header_images->directory}/thumbnails/{$header_images->filename}");
					}
				}
				
				$header_images->path = $upload["path"];
				$header_images->directory = $upload["directory"];
				$header_images->filename = $upload["filename"];
			}

			if($header_images->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A header image has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$header_images = HeaderImage::find($id);

			if (!$header_images) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$header_images->directory}/{$header_images->filename}")){
				File::delete("{$header_images->directory}/{$header_images->filename}");
			}
			if (File::exists("{$header_images->directory}/resized/{$header_images->filename}")){
				File::delete("{$header_images->directory}/resized/{$header_images->filename}");
			}
			if (File::exists("{$header_images->directory}/thumbnails/{$header_images->filename}")){
				File::delete("{$header_images->directory}/thumbnails/{$header_images->filename}");
			}

			if($header_images->save() AND $header_images->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A header image has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}