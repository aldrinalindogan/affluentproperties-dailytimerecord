<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Member;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\MemberRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class MemberController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Members";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "members";

		$this->data['member_types'] = ['' => "Choose member type", 'member' => "Member", 'founding_member' => "Founding Member", 'lead_staffers' => "Lead Staffers"];
	}

	public function index () {
		$this->data['members'] = Member::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (MemberRequest $request) {
		try {
			$new_member = new Member;
			$new_member->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/members');
				$new_member->path = $upload["path"];
				$new_member->directory = $upload["directory"];
				$new_member->filename = $upload["filename"];
			}

			if($new_member->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New member has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$member = Member::find($id);

		if (!$member) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['members'] = $member;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (MemberRequest $request, $id = NULL) {
		try {
			$member = Member::find($id);

			if (!$member) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$member->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/members');
				if($upload){	
					if (File::exists("{$member->directory}/{$member->filename}")){
						File::delete("{$member->directory}/{$member->filename}");
					}
					if (File::exists("{$member->directory}/resized/{$member->filename}")){
						File::delete("{$member->directory}/resized/{$member->filename}");
					}
					if (File::exists("{$member->directory}/thumbnails/{$member->filename}")){
						File::delete("{$member->directory}/thumbnails/{$member->filename}");
					}
				}
				
				$member->path = $upload["path"];
				$member->directory = $upload["directory"];
				$member->filename = $upload["filename"];
			}

			if($member->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A member has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$member = Member::find($id);

			if (!$member) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$member->directory}/{$member->filename}")){
				File::delete("{$member->directory}/{$member->filename}");
			}
			if (File::exists("{$member->directory}/resized/{$member->filename}")){
				File::delete("{$member->directory}/resized/{$member->filename}");
			}
			if (File::exists("{$member->directory}/thumbnails/{$member->filename}")){
				File::delete("{$member->directory}/thumbnails/{$member->filename}");
			}

			if($member->save() AND $member->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A member has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}