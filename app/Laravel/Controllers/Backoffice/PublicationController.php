<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Publication;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\PublicationRequest;
use App\Laravel\Requests\Backoffice\EditPublicationRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class PublicationController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Publication";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "publications";
	}

	public function index () {
		$this->data['publications'] = Publication::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (PublicationRequest $request) {
		try {
			$new_publication = new Publication;
			$new_publication->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/publication');
				$new_publication->path = $upload["path"];
				$new_publication->directory = $upload["directory"];
				$new_publication->filename = $upload["filename"];
			}

			if($new_publication->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New publication has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$publication = Publication::find($id);

		if (!$publication) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['publication'] = $publication;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditPublicationRequest $request, $id = NULL) {
		try {
			$publication = Publication::find($id);

			if (!$publication) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$publication->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/publication');
				if($upload){	
					if (File::exists("{$publication->directory}/{$publication->filename}")){
						File::delete("{$publication->directory}/{$publication->filename}");
					}
					if (File::exists("{$publication->directory}/resized/{$publication->filename}")){
						File::delete("{$publication->directory}/resized/{$publication->filename}");
					}
					if (File::exists("{$publication->directory}/thumbnails/{$publication->filename}")){
						File::delete("{$publication->directory}/thumbnails/{$publication->filename}");
					}
				}
				
				$publication->path = $upload["path"];
				$publication->directory = $upload["directory"];
				$publication->filename = $upload["filename"];
			}

			if($publication->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A publication has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$publication = Publication::find($id);

			if (!$publication) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$publication->directory}/{$publication->filename}")){
				File::delete("{$publication->directory}/{$publication->filename}");
			}
			if (File::exists("{$publication->directory}/resized/{$publication->filename}")){
				File::delete("{$publication->directory}/resized/{$publication->filename}");
			}
			if (File::exists("{$publication->directory}/thumbnails/{$publication->filename}")){
				File::delete("{$publication->directory}/thumbnails/{$publication->filename}");
			}

			if($publication->save() AND $publication->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A publication has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}