<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\VideoGallery;
use App\Laravel\Models\Album;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\VideoGalleryRequest;
use App\Laravel\Requests\Backoffice\EditVideoGalleryRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class VideoGalleryController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Video Gallery";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "video_gallery";
		$this->data['albums'] = ['' => "Choose an album for this image"] + Album::all()->pluck('title','id')->toArray();
	}

	public function index () {
		$this->data['videos'] = VideoGallery::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (VideoGalleryRequest $request) {
		try {
			$new_video = new VideoGallery;
			$new_video->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/video_gallery');
				$new_video->path = $upload["path"];
				$new_video->directory = $upload["directory"];
				$new_video->filename = $upload["filename"];
			}

			if($new_video->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New video has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$video_gallery = VideoGallery::find($id);

		if (!$video_gallery) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['video'] = $video_gallery;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditVideoGalleryRequest $request, $id = NULL) {
		try {
			$video_gallery = VideoGallery::find($id);

			if (!$video_gallery) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$video_gallery->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/Gallery');
				if($upload){	
					if (File::exists("{$video_gallery->directory}/{$video_gallery->filename}")){
						File::delete("{$video_gallery->directory}/{$video_gallery->filename}");
					}
					if (File::exists("{$video_gallery->directory}/resized/{$video_gallery->filename}")){
						File::delete("{$video_gallery->directory}/resized/{$video_gallery->filename}");
					}
					if (File::exists("{$video_gallery->directory}/thumbnails/{$video_gallery->filename}")){
						File::delete("{$video_gallery->directory}/thumbnails/{$video_gallery->filename}");
					}
				}
				
				$video_gallery->path = $upload["path"];
				$video_gallery->directory = $upload["directory"];
				$video_gallery->filename = $upload["filename"];
			}

			if($video_gallery->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A video has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$video_gallery = VideoGallery::find($id);

			if (!$video_gallery) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$video_gallery->directory}/{$video_gallery->filename}")){
				File::delete("{$video_gallery->directory}/{$video_gallery->filename}");
			}
			if (File::exists("{$video_gallery->directory}/resized/{$video_gallery->filename}")){
				File::delete("{$video_gallery->directory}/resized/{$video_gallery->filename}");
			}
			if (File::exists("{$video_gallery->directory}/thumbnails/{$video_gallery->filename}")){
				File::delete("{$video_gallery->directory}/thumbnails/{$video_gallery->filename}");
			}

			if($video_gallery->save() AND $video_gallery->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A video has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}