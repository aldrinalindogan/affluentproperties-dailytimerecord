<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Flag;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\FlagRequest;
use App\Laravel\Requests\Backoffice\EditFlagRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class FlagController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Flags";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "flags";
	}

	public function index () {
		$this->data['flags'] = Flag::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (FlagRequest $request) {
		try {
			$new_partners = new Flag;
			$new_partners->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/flags');
				$new_partners->path = $upload["path"];
				$new_partners->directory = $upload["directory"];
				$new_partners->filename = $upload["filename"];
			}

			if($new_partners->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New flag has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$flags = Flag::find($id);

		if (!$flags) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['flag'] = $flags;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditFlagRequest $request, $id = NULL) {
		try {
			$flags = Flag::find($id);

			if (!$flags) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$flags->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/flags');
				if($upload){	
					if (File::exists("{$flags->directory}/{$flags->filename}")){
						File::delete("{$flags->directory}/{$flags->filename}");
					}
					if (File::exists("{$flags->directory}/resized/{$flags->filename}")){
						File::delete("{$flags->directory}/resized/{$flags->filename}");
					}
					if (File::exists("{$flags->directory}/thumbnails/{$flags->filename}")){
						File::delete("{$flags->directory}/thumbnails/{$flags->filename}");
					}
				}
				
				$flags->path = $upload["path"];
				$flags->directory = $upload["directory"];
				$flags->filename = $upload["filename"];
			}

			if($flags->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A flag has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$flags = Flag::find($id);

			if (!$flags) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$flags->directory}/{$flags->filename}")){
				File::delete("{$flags->directory}/{$flags->filename}");
			}
			if (File::exists("{$flags->directory}/resized/{$flags->filename}")){
				File::delete("{$flags->directory}/resized/{$flags->filename}");
			}
			if (File::exists("{$flags->directory}/thumbnails/{$flags->filename}")){
				File::delete("{$flags->directory}/thumbnails/{$flags->filename}");
			}

			if($flags->save() AND $flags->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A flag has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}