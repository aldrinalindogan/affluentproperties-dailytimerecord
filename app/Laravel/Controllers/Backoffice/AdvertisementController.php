<?php namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Advertisement;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AdvertisementRequest;
use App\Laravel\Requests\Backoffice\EditAdvertisementRequest;

/**
*
* Classes used for this controller
*/
use App\Http\Requests\Request;
use Input, Helper, Carbon, Session, Str, File, Image, ImageUploader;

class AdvertisementController extends Controller{

	/**
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		parent::__construct();
		$view = Input::get('view','table');
		array_merge($this->data, parent::get_data());
		$this->data['page_title'] = "Advertisement";
		$this->data['page_description'] = "This is the general information about ".$this->data['page_title'].".";
		$this->data['route_file'] = "advertisements";

		$this->data['types'] = ['' => 'Choose location type','side' => 'Side', 'head' => 'Header'];
	}

	public function index () {
		$this->data['advertisements'] = Advertisement::orderBy('created_at',"DESC")->get();
		return view('backoffice.'.$this->data['route_file'].'.index',$this->data);
	}

	public function create () {
		return view('backoffice.'.$this->data['route_file'].'.create',$this->data);
	}

	public function store (AdvertisementRequest $request) {
		try {
			$new_advertisement = new Advertisement;
			$new_advertisement->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/advertisement');
				$new_advertisement->path = $upload["path"];
				$new_advertisement->directory = $upload["directory"];
				$new_advertisement->filename = $upload["filename"];
			}

			if($new_advertisement->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New advertisement has been added.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$advertisement = Advertisement::find($id);

		if (!$advertisement) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
		}

		$this->data['advertisement'] = $advertisement;
		return view('backoffice.'.$this->data['route_file'].'.edit',$this->data);
	}

	public function update (EditAdvertisementRequest $request, $id = NULL) {
		try {
			$advertisement = Advertisement::find($id);

			if (!$advertisement) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			$advertisement->fill($request->all());

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file,'storage/advertisement');
				if($upload){	
					if (File::exists("{$advertisement->directory}/{$advertisement->filename}")){
						File::delete("{$advertisement->directory}/{$advertisement->filename}");
					}
					if (File::exists("{$advertisement->directory}/resized/{$advertisement->filename}")){
						File::delete("{$advertisement->directory}/resized/{$advertisement->filename}");
					}
					if (File::exists("{$advertisement->directory}/thumbnails/{$advertisement->filename}")){
						File::delete("{$advertisement->directory}/thumbnails/{$advertisement->filename}");
					}
				}
				
				$advertisement->path = $upload["path"];
				$advertisement->directory = $upload["directory"];
				$advertisement->filename = $upload["filename"];
			}

			if($advertisement->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A advertisement has been updated.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$advertisement = Advertisement::find($id);

			if (!$advertisement) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			if (File::exists("{$advertisement->directory}/{$advertisement->filename}")){
				File::delete("{$advertisement->directory}/{$advertisement->filename}");
			}
			if (File::exists("{$advertisement->directory}/resized/{$advertisement->filename}")){
				File::delete("{$advertisement->directory}/resized/{$advertisement->filename}");
			}
			if (File::exists("{$advertisement->directory}/thumbnails/{$advertisement->filename}")){
				File::delete("{$advertisement->directory}/thumbnails/{$advertisement->filename}");
			}

			if($advertisement->save() AND $advertisement->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A advertisement has been deleted.");
				return redirect()->route('backoffice.'.$this->data['route_file'].'.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

}