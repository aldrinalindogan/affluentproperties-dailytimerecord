 

@extends('backoffice._layouts.main')
<section class="content">
     
  <!-- Vertical Layout -->
      <div class="page-head">
            <h2 class="page-head-title">Edit {{$page_title}}</h2>
            <ol class="breadcrumb page-head-nav">
                <li>
                    <a href="{{route('backoffice.dashboard')}}">Home</a>
                </li>
                <li class="active">All Records</li>
            </ol>
        </div>
        <div class="panel-body">       
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        
                        <div class="body">
                            <form method="POST" action="" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                

                                <label for="fname">First Name</label>
                                <div class="form-group {{$errors->first('username') ? 'has-error' : NULL}}">
                                    <div class="form-line">
                                        <input type="text" id="fname" name = "fname" class="form-control" placeholder="Enter your First Name" value="{{old('fname',$employee->fname)}}">
                                            @if($errors->first('fname'))
                                            <span class="help-block">{!!$errors->first('fname')!!}</span>
                                            @endif

                                    </div>
                                </div>

                                <label for="mname">Middle Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="mname" name = "mname" class="form-control" placeholder="Enter your Middle Name" value="{{old('mname',$employee->mname)}}" >
                                        @if($errors->first('mname'))
                                            <span class="help-block">{!!$errors->first('mname')!!}</span>
                                        @endif
                                    </div>
                                </div>


                                <label for="lname">Last Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="lname" name = "lname" class="form-control" placeholder="Enter your Last Name" value="{{old('lname',$employee->lname)}}">
                                         @if($errors->first('lname'))
                                            <span class="help-block">{!!$errors->first('lname')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                <label for="age">Age</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="age" name = "age" class="form-control" placeholder="Enter your Age" value="{{old('age',$employee->age)}}">
                                         @if($errors->first('age'))
                                            <span class="help-block">{!!$errors->first('age')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                <label for="birthdate">Birthday</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="datepicker form-control" id="birthdate" name = "birthdate" placeholder="Enter your birthday" value="{{old('birthdate',$employee->birthdate)}}">
                                         @if($errors->first('birthday'))
                                            <span class="help-block">{!!$errors->first('birthday')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                
                               
                                <label for="gender">Gender</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="gender" name = "gender" class="form-control" placeholder="Enter your gender" value="{{old('gender',$employee->gender)}}">
                                        @if($errors->first('gender'))
                                            <span class="help-block">{!!$errors->first('gender')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                <label for="email">Email</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="email" name = "email" class="form-control" placeholder="Enter your email" value="{{old('email',$employee->email)}}">
                                         @if($errors->first('email'))
                                            <span class="help-block">{!!$errors->first('email')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                <label for="address">Address</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="address" name = "address" class="form-control" placeholder="Enter your address" value="{{old('address',$employee->address)}}">
                                         @if($errors->first('address'))
                                            <span class="help-block">{!!$errors->first('address')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                <label for="contact">Mobile Number</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="contact" name = "contact" class="form-control" placeholder="Enter your mobile" value="{{old('contact',$employee->contact)}}">
                                         @if($errors->first('contact'))
                                            <span class="help-block">{!!$errors->first('contact')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                 <label for="position">Position</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="position" name = "position" class="form-control" placeholder="Enter your position" value="{{old('position',$employee->position)}}">
                                         @if($errors->first('position'))
                                            <span class="help-block">{!!$errors->first('position')!!}</span>
                                        @endif
                                    </div>
                                </div>
                                <button class="btn btn-space btn-primary" type="submit">Submit</button> 
                                <a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-space btn-default">Cancel</a>

                            </form>
                             
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout --> 

@section('page-modals')
    <div id="md-footer-danger" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                        <h3>Danger!</h3>
                        <p>This action can not be undone.<br>You are about to delete a record, this action can no longer be undone,<br> are you sure you want to proceed?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    <a type="button" class="btn btn-danger" id="btn-confirm-delete">Proceed</a>
                </div>
            </div>
        </div>
    </div>
@stop
 
        </div>
    </section>

@section('page-styles')
<!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('adminsite/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
     
    <!-- Waves Effect Css -->
    <link href="{{asset('adminsite/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('adminsite/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{asset('adminsite/plugins/morrisjs/morris.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('adminsite/css/style.css')}}" rel="stylesheet">

    <!-- AdminsiteBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('adminsite/css/themes/all-themes.css')}}" rel="stylesheet" />
@stop

@section('page-scripts')
 <!-- Jquery Core Js -->
    <script src="{{asset('adminsite/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('adminsite/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{asset('adminsite/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('adminsite/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('adminsite/plugins/node-waves/waves.js')}}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{asset('adminsite/plugins/jquery-countto/jquery.countTo.js')}}"></script>

    <!-- Morris Plugin Js -->
    <script src="{{asset('adminsite/plugins/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/morrisjs/morris.js')}}"></script>
    
    <!-- ChartJs -->
    <script src="{{asset('adminsite/plugins/chartjs/Chart.bundle.js')}}"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="{{asset('adminsite/plugins/flot-charts/jquery.flot.js')}}"></script>
    <script src="{{asset('adminsite/plugins/flot-charts/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('adminsite/plugins/flot-charts/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('adminsite/plugins/flot-charts/jquery.flot.categories.js')}}"></script>
    <script src="{{asset('adminsite/plugins/flot-charts/jquery.flot.time.js')}}"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{asset('adminsite/plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>


     <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('adminsite/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminsite/plugins/flot-charts/jquery.flot.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('adminsite/js/admin.js')}}"></script>
    <script src="{{asset('adminsite/js/pages/index.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{asset('adminsite/js/demo.js')}}"></script>
@stop
    
