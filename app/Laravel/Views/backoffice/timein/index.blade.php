@extends('backoffice._layouts.main')
@section('page-styles')
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('adminsite/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('adminsite/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('adminsite/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{asset('adminsite/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{asset('adminsite/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('adminsite/css/themes/all-themes.css')}}" rel="stylesheet" />
@stop


@section('page-scripts')
    <!-- Jquery Core Js -->
    <script src="{{asset('adminsite/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('adminsite/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{asset('adminsite/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('adminsite/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('adminsite/plugins/node-waves/waves.js')}}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('adminsite/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('adminsite/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('adminsite/js/admin.js')}}"></script>
    <script src="{{asset('adminsite/js/pages/tables/jquery-datatable.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{asset('adminsite/js/demo.js')}}"></script>
@stop 

@section('content')
@if(AUTH::user()->type == "user")
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h1>
                    List of {{$page_title}}
                    
                </h1>
                <small>{{$page_description}}</small>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ALL RECORDS
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">#</th>
                                        
                                        <th>Date</th>
                                        <th>Time In</th>
                                        <th>Time Out</th>
                                        <th>Work Durations</th>
                                        <th>Remarks</th> 
                                    </tr>
                                    </thead>
                                     
                                      <tbody>
                                    @foreach($attendance as $index => $info)
                                    <tr class="odd gradeX">
                                        <td>{{$index+1}}</td>
                                        <td>{{$info->date}}</td>
                                        <td>{{$info->timein}}</td>
                                        <td>{{$info->timeout}}</td>
                                        <td>{{$info->workduration}}</td>
                                        <td>{{$info->remarks}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            
        </div>
    </section>

@endif

@if(AUTH::user()->type == "super_user")
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h1>
                    List of {{$page_title}}
                    
                </h1>
                <small>{{$page_description}}</small>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                ALL RECORDS
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                        <th style="width: 5%;">#</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Time In</th>
                                        <th>Time Out</th>
                                        <th>Work Durations</th>
                                        <th>Remarks</th>
                                        <th class="actions"></th> 
                                    </tr>
                                    </thead>
                                     
                                      <tbody>
                                    @foreach($attendance as $index => $info)
                                    <tr class="odd gradeX">
                                        <td>{{$index+1}}</td>
                                        <td>{{$info->name}}</td>
                                        <td>{{$info->date}}</td>
                                        <td>{{$info->timein}}</td>
                                        <td>{{$info->timeout}}</td>
                                        <td>{{$info->workduration}}</td>
                                        <td>{{$info->remarks}}</td>     
                                        <td class="actions">
                                            <div class="btn-group btn-hspace">
                                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                                <ul role="menu" class="dropdown-menu pull-right">
                                                    <li>
                                                        <a href="{{route('backoffice.'.$route_file.'.edit',$info->id)}}">Set As Absent</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-url="{{route('backoffice.'.$route_file.'.destroy',[$info->id])}}" data-target="#md-footer-danger" class="action-delete">Set As Late</a>
                                                    </li>

                                                    <li>
                                                        <a href="#" data-toggle="modal" data-url="{{route('backoffice.'.$route_file.'.destroy',[$info->id])}}" data-target="#md-footer-danger" class="action-delete">Set As NPA</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
            
        </div>
    </section>

@endif
@stop

 
