@extends('backoffice._layouts.main')
<section class="content">
     
  <!-- Vertical Layout -->
      <div class="page-head">
            <h2 class="page-head-title">Add {{$page_title}}</h2>
            <ol class="breadcrumb page-head-nav">
                <li>
                    <a href="{{route('backoffice.dashboard')}}">Home</a>
                </li>
                <li class="active">All Records</li>
            </ol>
        </div>
        <div class="panel-body">       
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        
                        <div class="body">
                            <form method="POST" action="" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="name" value="{{AUTH::user()->fname}} {{AUTH::user()->lname}}">
                                <input type="hidden" name= "status" value="{{$status}}">
                               
                                <label for="email_address">Leave Date</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="leavedate" name = "leavedate" class="datepicker form-control" placeholder="Enter your Leave Date" value="{{old('leavedate')}}">
                                    </div>
                                </div>
                                                         
                                <label for="middlename">Leave Category</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="leavecategory" name = "leavecategory" class="form-control" placeholder="Enter your Leave Category" value="{{old('leavecategory')}}" >
                                    </div>
                                </div>

 

                                <label for="password">Leave Reason</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="leavereason" name = "leavereason" class="form-control" placeholder="Enter your Leave Reason" value="{{old('leavereason')}}">
                                    </div>
                                </div>

                               
                                <button class="btn btn-space btn-primary" type="submit">Submit</button> 
                                <a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-space btn-default">Cancel</a>

                            </form>
                             
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout --> 
        </div>

 
 
        </div>
    </section>

@section('page-styles')
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('adminsite/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('adminsite/}plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('adminsite/}plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{asset('adminsite/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    
    <!-- Bootstrap Select Css -->
    <link href="{{asset('adminsite/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
     

    <!-- Custom Css -->
    <link href="{{asset('adminsite/css/style.css')}}" rel="stylesheet">


    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('adminsite/css/themes/all-themes.css')}}" rel="stylesheet" />
@stop

@section('page-scripts') 

    <!-- Jquery Core Js -->
    <script src="{{asset('adminsite/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('adminsite/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{asset('adminsite/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('adminsite/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('adminsite/plugins/node-waves/waves.js')}}"></script>

    <!-- Autosize Plugin Js -->
    <script src="{{asset('adminsite/plugins/autosize/autosize.js')}}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{asset('adminsite/plugins/momentjs/moment.js')}}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{asset('adminsite/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('adminsite/js/admin.js')}}"></script>
    <script src="{{asset('adminsite/js/pages/forms/basic-form-elements.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{asset('adminsite/js/demo.js')}}"></script>
@stop
 