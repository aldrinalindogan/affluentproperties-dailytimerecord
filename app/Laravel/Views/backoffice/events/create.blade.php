@extends('backoffice._layouts.main')
<section class="content">
     
  <!-- Vertical Layout -->
      <div class="page-head">
            <h2 class="page-head-title">Add {{$page_title}}</h2>
            <ol class="breadcrumb page-head-nav">
                <li>
                    <a href="{{route('backoffice.dashboard')}}">Home</a>
                </li>
                <li class="active">All Records</li>
            </ol>
        </div>
        <div class="panel-body">       
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        
                        <div class="body">
                            <form method="POST" action="" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                
                                <label for="title">Meeting Title</label>
                                <div class="form-group {{$errors->first('title') ? 'has-error' : NULL}} xs-pt-10">
                                    <div class="form-line">
                                        <input type="text" id="title" name = "title" class="form-control input-sm" placeholder="Enter Title" value="{{old('title')}}">
                                    </div>
                                    @if($errors->first('title'))
                                    <span class="help-block">{!!$errors->first('title')!!}</span>
                                    @endif
                                </div>
                                 
                                <label for="time">Meeting Time</label>
                                <div class="form-group {{$errors->first('time') ? 'has-error' : NULL}}">
                                    <div class="form-line">
                                        <input type="text" id="time" name = "time" class="form-control" placeholder="Enter Time" value="{{old('time')}}">
                                        @if($errors->first('time'))
                                        <span class="help-block">{!!$errors->first('time')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                <label for="date">Meeting Date</label>
                                <div class="form-group {{$errors->first('date') ? 'has-error' : NULL}}">
                                    <div class="form-line">
                                        <input type="text" class="datepicker form-control" id="date" name = "date" placeholder="Enter Date" value="{{old('date')}}"">
                                        @if($errors->first('date'))
                                        <span class="help-block">{!!$errors->first('date')!!}</span>
                                        @endif
                                    </div>
                                </div>
  
                               
                                <label for="details">Meeting Details</label>
                                <div class="form-group {{$errors->first('details') ? 'has-error' : NULL}}">
                                    <div class="form-line">
                                        <textarea class="summernote" id="ckeditor" placeholder="Enter Details" type="text" name="details">{{old('details')}}</textarea>
 
                                        @if($errors->first('details'))
                                        <span class="help-block">{!!$errors->first('details')!!}</span>
                                        @endif
                                    </div>
                                </div>

 
                 
               
 
        
 
            <!-- #END# CKEditor -->

                                <label for="location">Meeting Location</label>
                                <div class="form-group {{$errors->first('location') ? 'has-error' : NULL}}">
                                    <div class="form-line">
                                        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCVjVd27vCiyVgWbRlAODLLRC8VpbN2-zc&libraries=places"></script>
                                        
                                        <input id="pac-input" class="form-control" type="text" name="location" value="{{old('location')}}" placeholder="Search the meeting location" style="width: 30%;" data-toggle="tooltip" title="{{$errors->first('location') ? $errors->first('location') : NULL}}">
                                        @if($errors->first('location'))
                                        <span class="help-block">{!!$errors->first('location')!!}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="container img-thumbnail" id="map-canvas" style="height:300px;"></div>
                                    
                                <button class="btn btn-space btn-primary" type="submit">Submit</button> 
                                <a href="{{route('backoffice.'.$route_file.'.index')}}" class="btn btn-space btn-default">Cancel</a>

                            </form>
                             
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout --> 
        </div>

 
 
        </div>
    </section>





@section('page-styles')
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('adminsite/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('adminsite/}plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('adminsite/}plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>

    <link rel="stylesheet" type="text/css" href="{{asset('backoffice/assets/lib/daterangepicker/css/daterangepicker.css')}}"/>
    
    <link href="{{asset('adminsite/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="{{asset('adminsite/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
     

    <!-- Custom Css -->
    <link href="{{asset('adminsite/css/style.css')}}" rel="stylesheet">


    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{asset('adminsite/css/themes/all-themes.css')}}" rel="stylesheet" />
@stop

@section('page-scripts') 

    <!-- Jquery Core Js -->
    <script src="{{asset('adminsite/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{asset('adminsite/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{asset('adminsite/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{asset('adminsite/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{asset('adminsite/plugins/node-waves/waves.js')}}"></script>

    <!-- Autosize Plugin Js -->
    <script src="{{asset('adminsite/plugins/autosize/autosize.js')}}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{asset('adminsite/plugins/momentjs/moment.js')}}"></script>

    <script src="{{asset('adminsite/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>




       <!-- Custom Js -->
    <script src="{{asset('adminsite/js/admin.js')}}"></script>
    <script src="{{asset('adminsite/js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('adminsite/js/pages/forms/editors.js')}}"></script>
     <!-- Ckeditor -->
    <script src="{{asset('adminsite/plugins/ckeditor/ckeditor.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{asset('adminsite/js/demo.js')}}"></script>

    <script type="text/javascript">   
     $(document).ready(function(){
        //initialize the javascript
        App.init();
        App.formElements();
        });
    </script>
<link href="{{ asset('backoffice/summernote/summernote.css')}}" rel="stylesheet"> 
<script src="{{ asset('backoffice/summernote/summernote.min.js')}}"></script> 
{{-- <script src="{{asset('frontend/summernote/summernote-cleaner.js')}}"></script> --}}
<script type="text/javascript">     
    $(function(){           
        $(".summernote").summernote({ 
            height : 300,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table',['table']],
                ['insert',['link','picture','video']],
                ['view',[/*'fullscreen',*/'codeview','help','undo','redo']]
            ],
            fontNames:["Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana","Roboto"],

            onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            },
            cleaner:{
                 notTime: 2400, // Time to display Notifications.
                 action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                 newline: '<br>', // Summernote's default is to use '<p><br></p>'
                 notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                 icon: '<i class="note-icon">Reset</i>',
                 keepHtml: false, // Remove all Html formats
                 keepClasses: false, // Remove Classes
                 badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                 badAttributes: ['style', 'start'] // Remove attributes from remaining tags
            },
            dialogsFade: true,
            dialogsInBody: true,
            placeholder: 'Description'
        }); 

        function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_KEY')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                 if(data.status == true){
                  $('.summernote').summernote('insertImage', data.image);
                 }
                }
            });
        }       
    }); 
</script>
<script type="text/javascript">
     function init() {
   var map = new google.maps.Map(document.getElementById('map-canvas'), {
     center: {
       lat: 14.599512,
       lng: 120.984222
     },
     zoom: 12
   });


   var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
   map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
   google.maps.event.addListener(searchBox, 'places_changed', function() {
     searchBox.set('map', null);


     var places = searchBox.getPlaces();

     var bounds = new google.maps.LatLngBounds();
     var i, place;
     for (i = 0; place = places[i]; i++) {
       (function(place) {
         var marker = new google.maps.Marker({

           position: place.geometry.location
         });
         marker.bindTo('map', searchBox, 'map');
         google.maps.event.addListener(marker, 'map_changed', function() {
           if (!this.getMap()) {
             this.unbindAll();
           }
         });
         bounds.extend(place.geometry.location);


       }(place));

     }
     map.fitBounds(bounds);
     searchBox.set('map', map);
     map.setZoom(Math.min(map.getZoom(),12));

   });
 }
 google.maps.event.addDomListener(window, 'load', init);
</script>
@stop
 
