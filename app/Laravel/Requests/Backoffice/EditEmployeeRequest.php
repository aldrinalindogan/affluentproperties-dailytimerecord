<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class EditEmployeeRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'fname' => "required",
			'mname' => "required",
			'lname' => "required",
			'age' => "required",
			'birthdate' => "required",
			'gender' => "required",
			'email' => "required",
			'address' => "required",
			'contact' => "required",
			'position' => "required"
			// 'contact' => "required",
			// 'address' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "Field is required.",
			 
		];
	}
}