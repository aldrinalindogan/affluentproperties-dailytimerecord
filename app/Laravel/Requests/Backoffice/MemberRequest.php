<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class MemberRequest extends RequestManager{

	public function rules(){

		$rules = [
			'title' => "required",
			'member_type' => "required",
			'details' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}