<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class LeavessRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'leavedate' => "required",
			'leavecategory' => "required",
			'leavereason' => "required",
			 
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "Field is required.",
		 
		];
	}
}