<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class EditEventRequest extends RequestManager{

	public function rules(){

		$rules = [
			'title' => "required",
			'date' => "required",
			'time' => "required",
			'details' => "required",
			'location' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}