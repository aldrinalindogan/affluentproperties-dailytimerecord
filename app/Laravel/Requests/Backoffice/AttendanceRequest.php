<?php namespace App\Laravel\Requests\Backoffice;

use Session,Auth, Input;
use App\Laravel\Requests\RequestManager;

class AttendanceRequest extends RequestManager{

	public function rules(){

		$rules = [
			'name' => "required",
			'date' => "required",
			'timein' => "required",
			'remarks' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
		];
	}
}